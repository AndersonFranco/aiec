package com.francodev.vendingmachine;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.SuperscriptSpan;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.felhr.usbserial.UsbSerialDevice;
import com.felhr.usbserial.UsbSerialInterface;
import com.google.gson.JsonObject;
import com.koushikdutta.ion.Ion;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends Activity {

    ImageButton buttons[];
    boolean itemRunning = false;

    MediaPlayer mediaPlayer;

    TextView temperatureTextView;
    float temperature = 0f;

    // USB

    public final String ACTION_USB_PERMISSION = "com.hariharan.arduinousb.USB_PERMISSION";
    UsbManager usbManager;
    UsbDevice device;
    UsbSerialDevice serialPort;
    UsbDeviceConnection connection;

    public String buffer = "";

    UsbSerialInterface.UsbReadCallback mCallback = new UsbSerialInterface.UsbReadCallback() { //Defining a Callback which triggers whenever data is read.
        @Override
        public void onReceivedData(byte[] arg0) {
            String data = null;
            try {
                data = new String(arg0, "UTF-8");

                processBuffer(data);

                Log.d("SERIAL-CALLBACK", data);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    };

    protected void processBuffer(String data) {

        String temperatureRegEx = "([+-]?\\d+\\.\\d{2})";

        buffer += data;

        if (buffer.length() > 3 && buffer.substring(0, 3).equals("t1:")) {
            try {
                Pattern pattern = Pattern.compile(temperatureRegEx);
                Matcher matcher = pattern.matcher(buffer);
                if (matcher.find()) {
                    temperature = Float.parseFloat(matcher.group());
                    Log.d("Temperatura", "Recebida: " + temperature);
                }
            } catch (Exception e) {}
            buffer = "";
        } else if (buffer.length() > 10) {
            buffer = buffer.substring(1);
        }

        Log.d("Buffer", buffer);

    }

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() { //Broadcast Receiver to automatically start and stop the Serial connection.
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ACTION_USB_PERMISSION)) {
                boolean granted = intent.getExtras().getBoolean(UsbManager.EXTRA_PERMISSION_GRANTED);
                if (granted) {
                    connection = usbManager.openDevice(device);
                    serialPort = UsbSerialDevice.createUsbSerialDevice(device, connection);
                    if (serialPort != null) {
                        if (serialPort.open()) { //Set Serial Connection Parameters.
                            //setUiEnabled(true);
                            serialPort.setBaudRate(9600);
                            serialPort.setDataBits(UsbSerialInterface.DATA_BITS_8);
                            serialPort.setStopBits(UsbSerialInterface.STOP_BITS_1);
                            serialPort.setParity(UsbSerialInterface.PARITY_NONE);
                            serialPort.setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF);
                            serialPort.read(mCallback);
                            Log.d("SERIAL", "Serial Connection Opened!");
                        } else {
                            Log.d("SERIAL", "PORT NOT OPEN");
                        }
                    } else {
                        Log.d("SERIAL", "PORT IS NULL");
                    }
                } else {
                    Log.d("SERIAL", "PERM NOT GRANTED");
                }
            } else if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED)) {
                onUSBDeviceAttached();
            } else if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_DETACHED)) {
                onUSBDeviceDetached();
            }
        }
    };
    private void onUSBDeviceAttached() {
        Log.d("SERIAL", "onUSBDeviceAttached called!");
        HashMap<String, UsbDevice> usbDevices = usbManager.getDeviceList();
        if (!usbDevices.isEmpty()) {
            boolean keep = true;
            for (Map.Entry<String, UsbDevice> entry : usbDevices.entrySet()) {
                device = entry.getValue();
                int deviceVID = device.getVendorId();
                if (deviceVID == 0x2341) //Arduino Vendor ID
                {
                    PendingIntent pi = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
                    usbManager.requestPermission(device, pi);
                    keep = false;
                } else {
                    connection = null;
                    device = null;
                }

                if (!keep)
                    break;
            }
        }
    }
    private void onUSBDeviceDetached() {
        serialPort.close();
        Log.d("SERIAL", "Serial Connection Closed!");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // USB
        usbManager = (UsbManager) getSystemService(this.USB_SERVICE);
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(broadcastReceiver, filter);
        onUSBDeviceAttached();

        // Carrega o som de clique dos botoes
        mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.button);

        // Carrega a fonte 7 Segment Display
        Typeface typeface = Typeface.createFromAsset(getAssets(), "DSEG7-Classic-MINI/DSEG7ClassicMini-Bold.ttf");

        //
        temperatureTextView = (TextView) findViewById(R.id.temperatureTextView);
        temperatureTextView.setTextColor(Color.parseColor("#90df25"));
        temperatureTextView.setTypeface(typeface);

        // Faz a temperatura piscar
        new Timer().scheduleAtFixedRate(new TimerTask() {
            boolean celsius = false;
            int cycles = 0;

            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // a cada 10 ciclos de 750ms, solicita nova temperatura
                        cycles = 1 + cycles % 10;
                        if (cycles == 10 && connection != null && serialPort != null && !itemRunning) {
                            Log.d("Temperatura", "Solicitando");
                            serialPort.write(String.format("%d\n", 6).getBytes());
                        }

                        // Formata o valor da temperatura
                        Spannable temperatureText = new SpannableString(String.format("%.1f°", temperature));

                        // Posiciona corretamente o simbolo de grau celsius
                        temperatureText.setSpan(new TextAppearanceSpan(null, 0, 88, null, null), temperatureText.length()-1, temperatureText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        temperatureText.setSpan(new SuperscriptSpan(), temperatureText.length()-1, temperatureText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                        if (celsius) {
                            temperatureText.setSpan(new ForegroundColorSpan(Color.parseColor("#90df25")), temperatureText.length() - 1, temperatureText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        } else {
                            temperatureText.setSpan(new ForegroundColorSpan(Color.DKGRAY), temperatureText.length() - 1, temperatureText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        }
                        celsius = !celsius;

                        temperatureTextView.setText(temperatureText);
                    }
                });
            }
        }, 0, 750);

        // Atribui o metodo click a cada botao
        buttons = new ImageButton[4];

        buttons[0] = (ImageButton) findViewById(R.id.buttonOne);
        buttons[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClicked(0);
            }
        });

        buttons[1] = (ImageButton) findViewById(R.id.buttonTwo);
        buttons[1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClicked(1);
            }
        });

        buttons[2] = (ImageButton) findViewById(R.id.buttonThree);
        buttons[2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClicked(2);
            }
        });

        buttons[3] = (ImageButton) findViewById(R.id.buttonFour);
        buttons[3].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClicked(3);
            }
        });

    }

    private void enableItem(int opc) {
        buttons[1].setEnabled(true);
        buttons[1].setAlpha(1f);
    }

    private void disableItem(int opc) {
        buttons[opc].setEnabled(false);
        buttons[opc].setAlpha(0.1f);
    }

    private void itemClicked(int opc) {

        if (itemRunning) return;

        itemRunning = true;

        final int selected = opc;

        mediaPlayer.start();

        // USB
        if (connection != null) {
            serialPort.write(String.format("%d\n", selected + 1).getBytes());
        }

        // Escurece os botoes, com excecao do botao que recebeu o clique
        for(int i = 0; i < 4; i++) {
            if (i != selected && buttons[i].isEnabled()) {
                buttons[i].setAlpha(0.25f);
            }
        }

        // Faz o botao que recebeu o clique piscar e, ao final, retorna todos os botoes disponiveis
        CountDownTimer timer = new CountDownTimer(2_100, 500) {
            @Override
            public void onTick(long millisUntilFinished) {
                if (Math.floor(millisUntilFinished / 500) % 2 == 0) {
                    buttons[selected].setAlpha(1f);
                } else {
                    buttons[selected].setAlpha(0.25f);
                }
            }

            @Override
            public void onFinish() {
                for(int i = 0; i < 4; i++) {
                    if (buttons[i].isEnabled()) {
                        buttons[i].setAlpha(1f);
                    }
                }

                itemRunning = false;
            }
        }.start();

        // Registra a venda no servidor central
        JsonObject json = new JsonObject();
        json.addProperty("produto_id", selected + 1);
        Ion.with(getApplicationContext())
                .load("http://192.168.2.2/aiec/central/public/api/venda")
                .setJsonObjectBody(json)
                .asJsonObject();

    }

}
