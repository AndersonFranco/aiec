<?php

namespace App\Http\Controllers;

use App\Maquina;
use Illuminate\Http\Request;

class MaquinaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Maquina::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id = null)
    {
        $validation = $this->getValidationFactory()->make($request->input(), [
            'nome' => 'required|max:191',
            'local' => 'required|max:191'
        ]);

        if (!$validation->fails()) {
            $maquina = $id !== null ? Maquina::find($id) : new Maquina();
            $maquina->fill($request->input());
            $maquina->save();
            return ['success' => true, 'record' => $maquina];
        } else {
            return ['success' => false, 'errors' => $validation->errors()];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Maquina::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->store($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Maquina::destroy($id);
    }
}
