<?php

namespace App\Http\Controllers;

use App\Produto;
use App\Venda;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $records = Venda::where('created_at', '>=', \Carbon\Carbon::now()->subMonth())
            ->groupBy('date')->groupBy('produto_id')->orderBy('date')
            ->get(['produto_id', DB::raw('Date(created_at) as date'), DB::raw('COUNT(*) as total')]);

        $vendas = [];
        foreach ($records as $r) {
            $data = preg_replace('/(\d+)-(\d+)-(\d+)/', '$3/$2/$1', $r->date);
            if (!isset($vendas[$data])) $vendas[$data] = [];
            $vendas[$data][$r->produto_id] = $r->total;
        }

        $sequencia = 0;
        $vendas = array_map(function($key, $value) use (&$sequencia) {
            $value['id'] = ++$sequencia;
            $value['data'] = $key;
            return $value;
        }, array_keys($vendas), $vendas);

        $produtos = Produto::all();
        $precos = [];
        foreach ($produtos as $r) {
            $precos[$r->id] = $r->preco;
        }

        return ['precos' => $precos, 'vendas' => $vendas];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id = null)
    {
        $validation = $this->getValidationFactory()->make($request->input(), [
            'produto_id' => 'required|numeric'
        ]);

        if (!$validation->fails()) {
            $venda = $id !== null ? Venda::find($id) : new Venda();
            $venda->fill($request->input());
            $venda->save();
            return ['success' => true, 'record' => $venda];
        } else {
            return ['success' => false, 'errors' => $validation->errors()];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Venda::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->store($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return ['success' => (bool)Venda::destroy($id)];
    }
}
