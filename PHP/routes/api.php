<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

//

Route::resource('maquina', 'MaquinaController', ['except' => ['create', 'edit']]);

Route::resource('produto', 'ProdutoController', ['except' => ['create', 'edit']]);

Route::resource('venda', 'VendaController', ['except' => ['create', 'edit']]);

