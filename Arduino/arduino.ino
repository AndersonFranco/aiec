#include <Thread.h>
#include <ThreadController.h>
#include <TimerOne.h>

#include <OneWire.h> 
#include <DallasTemperature.h>
#define ONE_WIRE_BUS 2 
OneWire oneWire(ONE_WIRE_BUS); 
DallasTemperature sensors(&oneWire);

unsigned char relayPin[4] = {7,6,5,4};
String cmd = "";

class TemperatureSensorThread: public Thread
{
  public:
    float temperature1;
    float temperature2;
    int delayInMillis = 1500; // 750 min
    unsigned long lastTempRequest = 0;

    void run() {
      if (millis() - lastTempRequest >= delayInMillis) {
        temperature1 = sensors.getTempCByIndex(0);
        temperature2 = sensors.getTempCByIndex(1);
        sensors.requestTemperatures();
      }
      runned();
    }
};

TemperatureSensorThread tempThread = TemperatureSensorThread();

ThreadController threadController = ThreadController();

void timerCallback() {
  threadController.run();
}

void setup(void) 
{ 
  // Relay 1-4
  int i;
  
  for(i = 0; i < 4; i++) {
    pinMode(relayPin[i], OUTPUT);
  }
  
 Serial.begin(9600);
 
 sensors.begin();
 sensors.setWaitForConversion(false);
 sensors.requestTemperatures();

 tempThread.lastTempRequest = millis();
 tempThread.setInterval(5000);
 
 threadController.add(&tempThread);
 
 Timer1.initialize();
 Timer1.attachInterrupt(timerCallback);
 Timer1.start();
}

void loop(void) 
{ 
  String resultText;

  int cmdSelected;

  if (Serial.available()) {

    cmdSelected = Serial.parseInt();
    cmd = Serial.readStringUntil('\n');
    
    if (cmdSelected > 0 && cmdSelected < 5) {
      resultText = "Relay ";
      resultText += cmdSelected;
      resultText += " On";
      Serial.println(resultText);
      digitalWrite(relayPin[cmdSelected - 1], HIGH);
      delay(100);
      resultText = "Relay ";
      resultText += cmdSelected;
      resultText += " Off";
      Serial.println(resultText);
      digitalWrite(relayPin[cmdSelected - 1], LOW);
    } else if (cmdSelected == 6) {
      resultText = "t1:";
      resultText += tempThread.temperature1;
      Serial.println(resultText);
    } else if (cmdSelected == 7) {
      resultText = "t2:";
      resultText += tempThread.temperature2;
      Serial.println(resultText);
    } else {
      resultText = "Unknown ";
      resultText += cmdSelected;
      resultText += " command.";
      Serial.println(resultText);
    }

  }
  
} 
