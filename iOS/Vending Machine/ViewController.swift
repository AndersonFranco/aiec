//
//  ViewController.swift
//  Vending Machine
//
//  Created by Anderson Franco on 04/06/17.
//  Copyright © 2017 AIEC - Trabalho Final. All rights reserved.
//

import UIKit
import SwiftyJSON

class ViewController: UIViewController {
    
    var vendas: JSON = JSON.null
    
    var precos: JSON = JSON.null
    
    @IBOutlet var dia: UILabel!
    
    @IBOutlet var valorTotal: UILabel!
    
    @IBOutlet var produto1: UILabel!
    
    @IBOutlet var produto2: UILabel!
    
    @IBOutlet var produto3: UILabel!
    
    @IBOutlet var produto4: UILabel!
        
    override func viewDidLoad() {
        super.viewDidLoad()        
     
        dia.text = vendas["data"].stringValue
        
        let t1 = vendas["1"].doubleValue * precos["1"].doubleValue
        let t2 = vendas["2"].doubleValue * precos["2"].doubleValue
        let t3 = vendas["3"].doubleValue * precos["3"].doubleValue
        let t4 = vendas["4"].doubleValue * precos["4"].doubleValue
        
        valorTotal.text = "R$ " + String(t1 + t2 + t3 + t4)
        
        let q1 = vendas["1"].intValue
        let q2 = vendas["2"].intValue
        let q3 = vendas["3"].intValue
        let q4 = vendas["4"].intValue
        
        produto1.text = String(q1) + "x" + precos["1"].stringValue + " = " + String(t1)
        produto2.text = String(q2) + "x" + precos["2"].stringValue + " = " + String(t2)
        produto3.text = String(q3) + "x" + precos["3"].stringValue + " = " + String(t3)
        produto4.text = String(q4) + "x" + precos["4"].stringValue + " = " + String(t4)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

