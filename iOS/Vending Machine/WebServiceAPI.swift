//
//  WebServiceAPI.swift
//  Vending Machine
//
//  Created by Anderson Franco on 04/06/17.
//  Copyright © 2017 AIEC - Trabalho Final. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class WebServiceAPI {
    
    typealias onCompletion = (JSON, Int) -> ()
    
    var apiURL: URL = URL.init(string: "http://192.168.2.2/aiec/central/public/api/")!
    
    func request(method: Alamofire.HTTPMethod, path: String, params: Parameters?, completion: @escaping onCompletion) {
        
        var requestURL = apiURL
        requestURL.appendPathComponent(path)        
        
        Alamofire.request(requestURL, method: method, parameters: params).validate().responseJSON {
            
            response in
            
            let statusCode = response.response?.statusCode ?? 0            
            
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                completion(json, statusCode)
            case .failure(let error):
                completion(JSON.null, statusCode)
                print(error)
            }
            
        }
        
    }
    
}
