//
//  PageViewController.swift
//  Vending Machine
//
//  Created by Anderson Franco on 04/06/17.
//  Copyright © 2017 AIEC - Trabalho Final. All rights reserved.
//

import UIKit
import SwiftyJSON

class PageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    private(set) lazy var paginas: [UIViewController] = []

    func update() {

        let loadingView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loadingViewController")
        setViewControllers([loadingView], direction: .forward, animated: false, completion: nil)
        
        DispatchQueue.global().async {
            
            WebServiceAPI().request(method: .get, path: "venda", params: nil) {
                (r, statusCode) in
                
                for vendasDoDia in r["vendas"] {
                    self.paginas.append(self.newViewController(vendas: vendasDoDia.1, precos: r["precos"]))
                }
                
                if let firstView = self.paginas.last {
                    self.setViewControllers([firstView], direction: .forward, animated: true, completion: nil)
                }
            }
        }
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.update), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)

        dataSource = self
        delegate = self
        
        update()
    }
    
    private func newViewController(vendas: JSON, precos: JSON) -> UIViewController {
        
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as! ViewController
        
        viewController.precos = precos
        viewController.vendas = vendas
        
        return viewController
        
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = paginas.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard paginas.count > previousIndex else {
            return nil
        }
        
        return paginas[previousIndex]
        
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = paginas.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = paginas.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return paginas[nextIndex]
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
